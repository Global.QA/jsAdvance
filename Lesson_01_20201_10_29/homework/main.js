// Задание 1.

// Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
var color;
var hexColor;

getRandomIntFromRange = (min, max) => {
    min = Math.ceil(min); // вычисляет и возвращает наименьшее целое число, которое больше или равно переданному числу (округляет число вверх)
    max = Math.floor(max); // вычисляет и возвращает наибольшее целое число, которое меньше или равно переданному числу (округляет число вниз)
    return Math.floor(Math.random() * (max - min)) + min;
}

const setRandomColor = () => {
    color = [];
    for (let i = 0; i < 3; i++) {
        color.push(getRandomIntFromRange(0, 255));
    }
    document.body.style.background = `RGB(${color})`; // якщо будемо використовувати #HEX - цей рядок треба прибрати.
};
// Каждая перезагрузка страницы будет с новым цветом.
setRandomColor();


// Для написания используйте функцию на получение случайного целого числа,
// между минимальным и максимальным значением (Приложена снизу задания)

// + Бонус, повесить обработчик на кнопку через метод onClick

var button = document.createElement('button');
var buttonText = document.createTextNode('Click here to Generate new Background Color');
button.appendChild(buttonText);
button.onclick = () => {
    setRandomColor();
    setRandomHEXcolor(color);
};

document.body.appendChild(button);

// + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
const setRandomHEXcolor = (color) => {
    hexColor = "#";
    for(var k in color){
        color[k] = color[k].toString(16);
        hexColor = hexColor + color[k];
    }
    document.body.style.background = hexColor;
};

// + Бонус выводить полученый цвет по центру страницы. - ?
//
// Необходимо создать блок через createElement задать ему стили через element.style
// и вывести через appendChild или insertBefore
//
// Необходимые материалы:
//   Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
//   function getRandomIntInclusive(min, max) {
//     min = Math.ceil(min);
//     max = Math.floor(max);
//     return Math.floor(Math.random() * (max - min + 1)) + min;
//   }
//   __
//   Работа с цветом:
//   Вариант 1.
//     Исользовать element.style.background = 'rgb(r,g,b)';
//     где r,g,b случайное число от 0 до 255;
//
//   Вариант 2.
//     Исользовать element.style.background = '#RRGGBB';
//     где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
//     Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
//     Перевод в 16-ричную систему исчесления делается при помощи
//     метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,
//
//     var myNumber = '251'
//     myNumber.toString(16) // fb
